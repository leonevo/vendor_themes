PRODUCT_PACKAGES += \
    FaceLockDark \
    SettingsDark \
    SettingsIntelligenceDark \
    SystemDark \
    SystemNotificationsDark \
    SystemBlack \
    SystemNotificationsBlack \
    SystemUIDark \
    SystemUIBlack \
    DocumentsUIDark \
    DocumentsUIBlack \
    UpdatesDark \
    UpdatesBlack

# Accents
PRODUCT_PACKAGES += \
    BrownAccent \
    TealAccent \
    BlackAccent \
    YellowAccent \
    RedAccent \
    PurpleAccent \
    PinkAccent \
    OrangeAccent \
    GreenAccent \
    WhiteAccent \
    CyanAccent \

# QS tile styles
PRODUCT_PACKAGES += \
    QStileCircleTrim \
    QStileDefault \
    QStileDualToneCircle \
    QStileSquircleTrim \
    QStileCookie \
    QStileAttemptMountain \
    QStileCircleDual \
    QStileCircleGradient \
    QStileDottedCircle \
    QStileNinja \
    QStilePokesign \
    QStileWavey \
    QStileInk \
    QStileInkDrop \
    QStileSquaremedo \
    QStileOreo \
    QStileTeardrop \
    QStileOreoCircleTrim \
    QStileOreoSquircleTrim \
    QStilesCircleOutline \
    QStileHexagon \
    QStileStar \
    QStileSquare \
    QStileGear \
    QStileBadge \
    QStileBadgetwo \
    QStileSquircle \
    QStileDiamond \
    QStileNeonlike \
    QStileOOS \
    QStileTriangles \
    QStileDivided \
    QStileCosmos
